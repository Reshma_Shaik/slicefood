package com.example.s528761.slicefood;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by S528761 on 10/16/2017.
 */

public class ScheduleAdapter extends ArrayAdapter {
    public ScheduleAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull ArrayList<ScheduleEvent> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        TextView tv1 = (TextView) v.findViewById(R.id.dateSc1);
        TextView tv2 = (TextView) v.findViewById(R.id.idSc1);
        TextView tv3 = (TextView) v.findViewById(R.id.nameSc1);
        TextView tv4 = (TextView) v.findViewById(R.id.timeSc1);
        TextView tv5 = (TextView) v.findViewById(R.id.time2Sc1);
        ScheduleEvent even = (ScheduleEvent) getItem(position);
        tv1.setText(even.getDate());
        tv2.setText(even.getId());
        tv3.setText(even.getName());
        tv4.setText(even.getTime());
        tv5.setText(even.getTime2());

        return v;
    }
}
