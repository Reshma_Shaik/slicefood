package com.example.s528761.slicefood;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;

//Alekhya create the file


public class EmployeeLogin extends AppCompatActivity implements SignUp.signup, EmpLogInFragment.empLogIn{

    Fragment emp_log_fr;
    Fragment sign_up_fr;
    private boolean doAdd;
    private boolean firstShowing;
    private String[] usn;
    private String[] psw;
    private EditText userEdit;
    private EditText userPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_login);


        //Fragment declaration
        sign_up_fr = new SignUp();
        emp_log_fr = new EmpLogInFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction trans = fm.beginTransaction();
        //showing employee loin screen
        trans.add(R.id.frame1, emp_log_fr);
        trans.commit();


        //Validating
      /*  Intent i = getIntent();
        usn = i.getStringArrayExtra("username");
        psw = i.getStringArrayExtra("password");*/


    }


    //navigating to Start(Image button)
    public void slice(View v){
        Intent intent = new Intent(this,MainActivity.class );
        startActivity(intent);
    }
    //implementing singup
    @Override
    public void register(){
       // startActivity(new Intent(this, EmployeeLogin.class));
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction trans = fm.beginTransaction();
        trans.detach(sign_up_fr);
        //showing employee loin screen
        trans.add(R.id.frame1, emp_log_fr);
        trans.attach(emp_log_fr);
        trans.commit();
    }
    @Override
    public void registerSignin(){
        startActivity(new Intent(this, MainActivity.class));
    }
    //implementing emp login screen


 /*  @Override
    public void empLogScreen(){
        userEdit = (EditText) findViewById(R.id.userEtFL2);
        userPwd = (EditText)findViewById(R.id.passEtFL2);
        final String username = userEdit.getText().toString();
        final String password = userPwd.getText().toString();

        Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                Toast.makeText(getApplicationContext(), "Login Success!", Toast.LENGTH_SHORT).show();
                //Intent intent = new Intent(getApplicationContext(),MainMenu.class );
                //startActivity(intent);
            }


            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("TAG", username+"_"+password);
                Intent intent = new Intent(getApplicationContext(),MainMenu.class );
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Login-- failed!", Toast.LENGTH_SHORT).show();
            }
        });

    }*/



    //implementing emp signup screen
    @Override
    public void empSignupScreen(){
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        trans.detach(emp_log_fr);
        trans.add(R.id.frame1, sign_up_fr);
        trans.attach(sign_up_fr);
        trans.commit();

    }


    }
