package com.example.s528761.slicefood;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//Reshma Shaik created the file

public class SubSlips extends Activity {
    private ArrayList<SubslipEvent> data1 = new ArrayList<SubslipEvent>();
    private HashMap sub =new HashMap();
    private  EditText et;
    private  EditText et1;
    private  EditText et2;
    private  EditText et3;
    private  EditText et4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_slips);
        data1.add(new SubslipEvent("ID","Name","Department","Date","Time"));
        ArrayAdapter<ScheduleEvent> adapter = new SubSlipAdapter(this, R.layout.event_subslip,R.id.textView,data1);
        ListView listV = (ListView) findViewById(R.id.listView);
        listV.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        Button bt1 = (Button) findViewById(R.id.listclear);
        Button bt2 = (Button) findViewById(R.id.listback);
        Button bt = (Button) findViewById(R.id.listupdate);
        bt.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                et = (EditText) findViewById(R.id.listid);
                et1 = (EditText) findViewById(R.id.listname);
                et2 = (EditText) findViewById(R.id.listdept);
                et3 = (EditText) findViewById(R.id.listdate);
                et4 = (EditText) findViewById(R.id.listtime);
                String s = et.getText().toString();
                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();
                String s3 = et3.getText().toString();
                String s4 = et4.getText().toString();

                ArrayAdapter<ScheduleEvent> adapter = new SubSlipAdapter(getApplicationContext(), R.layout.event_subslip,R.id.textView,data1);
                ListView listV = (ListView) findViewById(R.id.listView);
                if(et.getText().toString().length()==0||et1.getText().toString().length()==0||et2.getText().toString().length()==0||
                        et3.getText().toString().length()==0||et4.getText().toString().length()==0){
                    Toast.makeText(getApplicationContext(),"Please Fill all the details to submit",Toast.LENGTH_SHORT).show();
                }else{
                    sub.put("id", s);
                    sub.put("name", s1);
                    sub.put("dept",s2);
                    sub.put("date1", s3);
                    sub.put("time", s4);
                    Backendless.Persistence.of( "subslip" ).save( sub, new AsyncCallback<Map>() {
                        public void handleResponse( Map response )
                        {
                            System.out.println(response);
                            // new Contact instance has been saved

                        }

                        public void handleFault( BackendlessFault fault )
                        {
                            System.out.println(fault);
                            // an error has occurred, the error code can be retrieved with fault.getCode()
                        }
                    });
                    data1.add(new SubslipEvent(s,s1,s2,s3,s4));
                    listV.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        bt1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) findViewById(R.id.listid);
                EditText et1 = (EditText) findViewById(R.id.listname);
                EditText et2 = (EditText) findViewById(R.id.listdept);
                EditText et3 = (EditText) findViewById(R.id.listdate);
                EditText et4 = (EditText) findViewById(R.id.listtime);
                et.setText("");
                et1.setText("");
                et2.setText("");
                et3.setText("");
                et4.setText("");


            }
        });
        bt2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainMenu.class);
                startActivity(intent);
            }
        });


    }

}