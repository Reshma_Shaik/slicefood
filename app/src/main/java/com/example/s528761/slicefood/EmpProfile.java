package com.example.s528761.slicefood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class EmpProfile extends AppCompatActivity {

    private String _name;
    private String _password;
    private String _phone;
    private String _username;
    private TextView t1;
    private TextView t2;
    private TextView t3;
    private TextView t4;
    private ArrayList<String>  na;
    private ArrayList<String>  ph;
    private ArrayList<String>  pas;
    private ArrayList<String>  us;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_profile);

        t1 = (TextView) findViewById(R.id.proName1);
        t2 = (TextView) findViewById(R.id.proID2);
        t3 = (TextView) findViewById(R.id.proRole2);
        t4 = (TextView) findViewById(R.id.proDept2);

        DataQueryBuilder queryBuilder = DataQueryBuilder.create();
        Backendless.Persistence.of( "schedule" ).find(new AsyncCallback<List<Map>>(){
            @Override
            public void handleResponse( List<Map> foundContacts )
            {
                for(int i=0; i< foundContacts.size(); i++) {
                    Log.i("MYAPP", "result" + foundContacts.get(i));//run
                }
               // every loaded object from the "Contact" table is now an individual java.util.Map
                for (Map<String, String> featureService : foundContacts) {

                    for (Map.Entry<String, String> entry : featureService.entrySet()) {
//                        Log.i("MYAPP", "KEY" + entry.getKey() + ": " + entry.getValue());

                            if (entry.getKey().equals("name")) {
                                //na.add(entry.getValue());
                                _name = entry.getValue();
                                t1.setText(entry.getValue());
                            }
                            if (entry.getKey().equals("phone")) {
                                //ph.add(entry.getValue());
                                _phone = entry.getValue();
                                t2.setText(entry.getValue());
                            }
//                        if(entry.getKey().equals("date1")){
//                            _event.setDate(entry.getValue());
//                        }
                            if (entry.getKey().equals("username")) {
                                //us.add(entry.getValue());
                                _username = entry.getValue();
                                t3.setText(entry.getValue());
                            }
                            if (entry.getKey().equals("password")) {
                                //pas.add(entry.getValue());
                                _password = entry.getValue();
                                t4.setText(entry.getValue());
                            }
                        /*if(!(_event.getId() == null)
                                //&& !(_event.getDate() == null)
                                &&
                                !(_event.getTime() == null)  && !(_event.getName() == null)  &&
                                !(_event.getTime2() == null)
                                ) {
                            _listOfSch.add(_event);
                        }//run*/
                        }
                    }


            }
            @Override
            public void handleFault( BackendlessFault fault )
            {
                // an error has occurred, the error code can be retrieved with fault.getCode()
            }
        });


    }
    public void proBackBT(View v){
        Intent intent = new Intent(this, MainMenu.class);
        startActivity(intent);
    }
}
