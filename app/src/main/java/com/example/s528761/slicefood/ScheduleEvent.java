package com.example.s528761.slicefood;

/**
 * Created by S528761 on 10/16/2017.
 */

public class ScheduleEvent {
    private String id;
    private String date;
    private String name;
    private String time;
    private String time2;

    public ScheduleEvent(String date, String id, String name, String time, String time2){
        this.date = date;
        this.id = id;
        this.name = name;
        this.time = time;
        this.time2 = time2;
    }
    public ScheduleEvent(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }


}
