package com.example.s528761.slicefood;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by s528761 on 10/27/2017.
 */
//Reshma shaik created the file
public class SubSlipAdapter extends ArrayAdapter {
    public SubSlipAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull ArrayList<SubslipEvent> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        TextView tv1 = (TextView) v.findViewById(R.id.textView);
        TextView tv2 = (TextView) v.findViewById(R.id.textView2);
        TextView tv3 = (TextView) v.findViewById(R.id.textView3);
        TextView tv4 = (TextView) v.findViewById(R.id.textView4);
        TextView tv5 = (TextView) v.findViewById(R.id.textView5);

        SubslipEvent even = (SubslipEvent) getItem(position);
        tv1.setText(even.getEveid());
        tv2.setText(even.getEvename());
        tv3.setText(even.getEvedept());
        tv4.setText(even.getEvedate());
        tv5.setText(even.getEvetime());

        return v;
    }
}
