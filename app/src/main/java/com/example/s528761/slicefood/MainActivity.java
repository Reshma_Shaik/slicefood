package com.example.s528761.slicefood;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;

//Reshma Shaik created the file
public class MainActivity extends AppCompatActivity {
    public static final String APP_ID = "80B4AEFD-9F7F-BE1E-FF20-216F0DA3BF00";
    public static final String SECRET_KEY = "4F9AA33F-8EA2-0729-FF18-D18D1F6BB700";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //backedless
        Backendless.initApp(this,APP_ID,SECRET_KEY);



        ImageButton userIb = (ImageButton)findViewById(R.id.userImgBTN);
        ImageButton adminIb = (ImageButton)findViewById(R.id.adminImgBTN);
        //network connection
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected()){


        }else{
            //Image buttons are set to false
            userIb.setEnabled(false);
            adminIb.setEnabled(false);
            //Alert dialog box for network connection
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Network Information");
            alertDialog.setMessage("No Internet connection available. Please turn on Internet connection");
            alertDialog.setPositiveButton("TurnOn WIFI", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

                    }
                });
            alertDialog.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                       finish();

                    }
                });

            alertDialog.show();

        }

    }
    //Navigating to employee login page
    public void userImage(View v){
        ImageButton userImg = (ImageButton)findViewById(R.id.userImgBTN);
        Intent intent = new Intent(this,EmployeeLogin.class);
        startActivity(intent);
    }
    //Navigating to admin login page
    public void adminImage(View v){

        Intent intent = new Intent(this,AdminLogin.class);
        startActivity(intent);
    }

}
