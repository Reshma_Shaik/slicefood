package com.example.s528761.slicefood;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUp extends Fragment {


    private String name1;
    private String phone1;
    private String username1;
    private String password1;
    HashMap Users = new HashMap();
    private EditText nET;
    private EditText pET;
    private EditText uET;
    private EditText uP;



    public SignUp() {
        // Required empty public constructor
    }
    public interface signup{
        public void register();
        public void registerSignin();

    }
    private signup signupActivity;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        signupActivity = (signup) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.sign_up, container, false);
        //backendless


        nET = (EditText) v.findViewById(R.id.nameET);
        pET = (EditText) v.findViewById(R.id.phoneET);
        uET = (EditText) v.findViewById(R.id.userET);
        uP = (EditText) v.findViewById(R.id.pwdET);

        Log.d("TAG","Values");
        Log.d("TAG",nET.getText().toString());
        Log.d("xx",pET.getText().toString());
        Log.d("xx",uET.getText().toString());
        Log.d("xx",uP.getText().toString());




        //Registration page

        Drawable userImg = getResources().getDrawable(R.drawable.useret);
        Drawable userPwdImg = getResources().getDrawable(R.drawable.locket);
        userImg.setBounds(0, 0, 65, 65);
        userPwdImg.setBounds(0, 0, 60, 60);
        uET.setCompoundDrawables(userImg, null, null, null);
        uP.setCompoundDrawables(userPwdImg, null, null, null);





       /* username.add(userEdit.getText().toString());
        password.add(userPwd.getText().toString());
        name.add(nameET.getText().toString());
        phone.add(phoneET.getText().toString());
        */






        //submit button with dialog box
        Button submit = (Button) v.findViewById(R.id.submit);
        submit.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("TAG","Database enterd");
                // Registering the user in the database
//                Backendless.initApp(this,APP_ID,SECRET_KEY,VERSION);
                //
                name1 = nET.getText().toString();
                phone1 = pET.getText().toString();
                username1 = uET.getText().toString();
                password1 = uP.getText().toString();
                if(password1.length()!=0&&name1.length()!=0&&username1.length()!=0&&phone1.length()!=0) {
                    Users.put("username", username1);
                    Users.put("name",name1);
                    Users.put("phone",phone1);
                    Users.put("password",password1);
//                    Backendless.Persistence.of( "UserService" ).save( Users, new AsyncCallback<Map>() {
//                        public void handleResponse( Map response )
//                        {
//                            System.out.println(response);
//                            // new Contact instance has been saved
//                            Toast.makeText(getActivity(), "Registered!", Toast.LENGTH_SHORT).show();
//                            Intent i = new Intent(getActivity(), EmployeeLogin.class);
//                            startActivity(i);
//                        }
//
//                        public void handleFault( BackendlessFault fault )
//                        {
//                            System.out.println(fault);
//                            Toast.makeText(getActivity(), "Not Registered!", Toast.LENGTH_SHORT).show();
//                            // an error has occurred, the error code can be retrieved with fault.getCode()
//                        }
//                    });

//


                    BackendlessUser user = new BackendlessUser();

                    user.setProperty("username", username1);
                    user.setProperty("phone", phone1);
                    user.setProperty("name", name1);
                    user.setPassword( password1 );


                    Backendless.UserService.register( user, new AsyncCallback<BackendlessUser>()
                    {
                        @Override
                        public void handleResponse( BackendlessUser backendlessUser )
                        {
                            Toast.makeText(getContext(), "Registration Successfull!", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getContext(), EmployeeLogin.class);
                            startActivity(i);
                        }

                        @Override
                        public void handleFault( BackendlessFault backendlessFault )
                        {
                            Toast.makeText(getContext(), "Registration unsuccessful!", Toast.LENGTH_SHORT).show();

                        }
                    } );

                }else{
                    Toast.makeText(getActivity(), "fill all the details", Toast.LENGTH_SHORT).show();
                }

            }
        });


        return  v;
    }



}