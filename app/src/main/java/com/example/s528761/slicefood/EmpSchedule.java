package com.example.s528761.slicefood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.IDataStore;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.backendless.BackendlessUser;
import com.backendless.persistence.QueryOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import weborb.util.ObjectProperty;


import static com.example.s528761.slicefood.R.id.map;
//Reshma Shaik created the file

public class EmpSchedule extends AppCompatActivity {

    private ArrayList<ScheduleEvent> data = new ArrayList<ScheduleEvent>();//this array list of type Schdule event
    private String[] temp;
    private List<ScheduleEvent> sched = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_schedule);

        data.add(new ScheduleEvent("ID","Date","Name","From","To"));
        //getting bundle values
        /*Map firstContact = Backendless.Persistence.of( "schedule" ).findFirst();

        Backendless.Persistence.of( "schedule" ).findFirst( new AsyncCallback<Map>(){
            @Override
            public void handleResponse( Map contact )

            {
                Log.d("TAG", "Data retrieved!");
                // first contact instance has been found
            }
            @Override
            public void handleFault( BackendlessFault fault )

            {
                Log.d("TAG", "Data not retrieved");
                // an error has occurred, the error code can be retrieved with fault.getCode( )
            }
        });
        */
        DataQueryBuilder queryBuilder = DataQueryBuilder.create();
       // queryBuilder.setWhereClause( "orderAmount > 100" );
       // List<Map> result  = Backendless.Persistence.of( "schedule" ).find();
//        Backendless.Data.of( "schedule" ).getObjectCount( queryBuilder,
//                new AsyncCallback<Integer>()
//                {
//                    @Override
//                    public void handleResponse( Integer integer )
//                    {
//                        Log.i( "MYAPP", "found objects " + integer );
//                    }
//
//                    @Override
//                    public void handleFault( BackendlessFault backendlessFault )
//                    {
//                        Log.i( "MYAPP","error-" + backendlessFault.getMessage() );
//                    }
//                } );

        Backendless.Persistence.of( "schedule" ).find( new AsyncCallback<List<Map>>(){
            @Override
            public void handleResponse( List<Map> foundContacts )
            {
                for(int i=0; i< foundContacts.size(); i++) {
                    Log.i("MYAPP", "result" + foundContacts.get(i));//run
                }
                ArrayList<ScheduleEvent> _listOfSch = new ArrayList<ScheduleEvent>();
                ArrayList<ScheduleEvent> _finalSet = new ArrayList<ScheduleEvent>();
                // every loaded object from the "Contact" table is now an individual java.util.Map
                for (Map<String, String> featureService : foundContacts) {
                    ScheduleEvent _event = new ScheduleEvent();
                    for (Map.Entry<String, String> entry : featureService.entrySet()) {
//                        Log.i("MYAPP", "KEY" + entry.getKey() + ": " + entry.getValue());

                        if(entry.getKey().equals("id")){
                            _event.setId(entry.getValue());
                        }
                        if(entry.getKey().equals("to")){
                            _event.setTime2(entry.getValue());
                        }
//                        if(entry.getKey().equals("date1")){
//                            _event.setDate(entry.getValue());
//                        }
                        if(entry.getKey().equals("name")){
                            _event.setName(entry.getValue());
                        }
                        if(entry.getKey().equals("from")){
                            _event.setTime(entry.getValue());
                        }
                        if(!(_event.getId() == null)
                                //&& !(_event.getDate() == null)
                                &&
                                !(_event.getTime() == null)  && !(_event.getName() == null)  &&
                                !(_event.getTime2() == null)
                                ) {
                            _listOfSch.add(_event);
                        }//run
                        HashSet<ScheduleEvent> _scheduleSet = new HashSet<ScheduleEvent>(_listOfSch);
                         _finalSet = new ArrayList<ScheduleEvent>(_scheduleSet);
                    }
                }
                Log.i("MYAPP", "result" + _listOfSch.get(1));
                ArrayAdapter<ScheduleEvent> adapter = new ScheduleAdapter(getApplicationContext(), R.layout.schedule_event,R.id.dateSc1,_finalSet);
                ListView listV = (ListView) findViewById(R.id.list);
                listV.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void handleFault( BackendlessFault fault )
            {
                // an error has occurred, the error code can be retrieved with fault.getCode()
            }
        });

        //ArrayAdapter<ScheduleEvent> adapter = new ScheduleAdapter(this, R.layout.schedule_event,R.id.dateSc1,data);
        //ListView listV = (ListView) findViewById(R.id.list);
        //listV.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

    }
     public void menuBT(View v){
         Intent intent = new Intent(this, MainMenu.class);
         startActivity(intent);
     }

}
