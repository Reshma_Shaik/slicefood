package com.example.s528761.slicefood;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;

//Alekhya created the file

public class MainMenu extends AppCompatActivity{

    protected Context context;
    private String tempVar_user;
    private String temVar_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Intent i = getIntent();
        tempVar_user = i.getStringExtra("username");
        temVar_pass = i.getStringExtra("password");
    }




    //navigate to Employee login
    public void logoutBT(View v){

                    Backendless.UserService.logout(new AsyncCallback<Void>() {
                        public void handleResponse(Void response) {
                            // user has been logged out.
                            Toast.makeText(getApplicationContext(), "Successfully logged out", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), EmployeeLogin.class);
                            startActivity(i);
                        }

                        public void handleFault(BackendlessFault fault) {
                            // something went wrong and logout failed, to get the error code call fault.getCode()
                        }
                    });

        }



    //Schedule button
    public void scBT(View v){

        startActivity(new Intent(this, EmpSchedule.class));
    }
    //profile button
    public void profile(View v){
        Intent intent = new Intent(this, EmpProfile.class);
               startActivity(intent);
    }
    public void punch(View v){
        Toast.makeText(getApplicationContext(),"PunchIn Success",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
    public void punchOut(View v){
        Toast.makeText(getApplicationContext(),"PunchOut Success",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
    public void sub(View v){
        Intent intent = new Intent(this, SubSlips.class);
        startActivity(intent);
    }
}
