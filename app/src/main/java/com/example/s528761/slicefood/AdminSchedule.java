package com.example.s528761.slicefood;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ListMenuPresenter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;


//Reshma Shaik created the file


public class AdminSchedule extends AppCompatActivity {
    ArrayList<ScheduleEvent> schedule =new ArrayList<ScheduleEvent>();
    private  String  fromtime;
    private String toTime;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    private String dateValue;
    Button dateP;
    public HashMap admin = new HashMap<>();
    private  EditText ed;
    private  EditText ed1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_schedule);



        //DatePicker
        dateP = (Button) findViewById(R.id.datepick);
        dateP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(AdminSchedule.this,
                        android.R.style.Theme_Holo_Dialog_MinWidth, onDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                i1 = i1 + 1;
                dateValue = i1+"/"+i2+"/"+i;
            }
        };


        Spinner from = (Spinner) findViewById(R.id.from);
        String[] from_item = new String[]{"From", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm"};
        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, from_item);
        from.setAdapter(adapter4);
        Spinner to = (Spinner) findViewById(R.id.to);
        String[] to_item = new String[]{"To", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm"};
        ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, to_item);
        to.setAdapter(adapter5);
        ListView list = (ListView) findViewById(R.id.lv);
        ArrayAdapter<ScheduleEvent> ad = new ScheduleAdapter(this, R.layout.schedule_event, R.id.nameSc1, schedule);

        list.setAdapter(ad);



        from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        fromtime = "6AM";
                        break;
                    case 1:
                        fromtime = "6AM";
                        break;
                    case 2:
                        fromtime = "7AM";
                        break;
                    case 3:
                        fromtime = "8AM";
                        break;
                    case 4:
                        fromtime = "9AM";
                        break;
                    case 5:
                        fromtime = "10AM";
                        break;
                    case 6:
                        fromtime = "11AM";
                        break;
                    case 7:
                        fromtime = "12PM";
                        break;
                    case 8:
                        fromtime = "1PM";
                        break;
                    case 9:
                        fromtime = "2PM";
                        break;
                    case 10:
                        fromtime = "3PM";
                        break;
                    case 11:
                        fromtime = "4PM";
                        break;
                    case 12:
                        fromtime = "5PM";
                        break;
                    case 13:
                        fromtime = "6PM";
                        break;
                    case 14:
                        fromtime = "7PM";
                        break;
                    case 15:
                        fromtime = "8PM";
                        break;
                    case 16:
                        fromtime = "9PM";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                fromtime = "6AM";
            }
        });


        to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        toTime = "6AM";
                        break;
                    case 1:
                        toTime = "6AM";
                        break;
                    case 2:
                        toTime = "7AM";
                        break;
                    case 3:
                        toTime = "8AM";
                        break;
                    case 4:
                        toTime = "9AM";
                        break;
                    case 5:
                        toTime = "10AM";
                        break;
                    case 6:
                        toTime = "11AM";
                        break;
                    case 7:
                        toTime = "12PM";
                        break;
                    case 8:
                        toTime = "1PM";
                        break;
                    case 9:
                        toTime = "2PM";
                        break;
                    case 10:
                        toTime = "3PM";
                        break;
                    case 11:
                        toTime = "4PM";
                        break;
                    case 12:
                        toTime = "5PM";
                        break;
                    case 13:
                        toTime = "6PM";
                        break;
                    case 14:
                        toTime = "7PM";
                        break;
                    case 15:
                        toTime = "8PM";
                        break;
                    case 16:
                        toTime = "9PM";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                toTime = "8AM";
            }
        });


        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                schedule.remove(i);
                ListView list = (ListView) findViewById(R.id.lv);
                ArrayAdapter<ScheduleEvent> ad = new ScheduleAdapter(getApplicationContext(), R.layout.schedule_event, R.id.nameSc1, schedule);
                list.setAdapter(ad);
                ad.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Deleted the row!",Toast.LENGTH_SHORT).show();
            }
        });



    }


        //navigating to menu
    public void menuBTAd(View v){
        Intent intent = new Intent(this, AdminMenu.class);
        startActivity(intent);
    }
    public void adup(View v){
        ed = (EditText) findViewById(R.id.userId);
        ed1 = (EditText) findViewById(R.id.userName);
        admin.put("id", ed.getText().toString());
        admin.put("name", ed1.getText().toString());
        admin.put("date1",dateValue);
        admin.put("from", fromtime);
        admin.put("to", toTime);
        Backendless.Persistence.of( "schedule" ).save( admin, new AsyncCallback<Map>() {
            public void handleResponse( Map response )
            {
                System.out.println(response);
                // new Contact instance has been saved

            }

            public void handleFault( BackendlessFault fault )
            {
                System.out.println(fault);

                // an error has occurred, the error code can be retrieved with fault.getCode()
            }
        });
        schedule.add(new ScheduleEvent(dateValue,ed.getText().toString(), ed1.getText().toString(), fromtime, toTime));
        ListView list = (ListView) findViewById(R.id.lv);
        ArrayAdapter<ScheduleEvent> ad = new ScheduleAdapter(this, R.layout.schedule_event, R.id.nameSc1, schedule);
        list.setAdapter(ad);


    }


}
