package com.example.s528761.slicefood;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.LoginFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.Map;

import static android.R.attr.password;


/**
 * A simple {@link Fragment} subclass.
 */

//Reshma Shaik created the file
public class EmpLogInFragment extends Fragment {

    private EditText et;
    private EditText et1;

    public EmpLogInFragment() {
        // Required empty public constructor
    }
    public interface empLogIn{
      //public void empLogScreen();
        public void empSignupScreen();
    }

    private empLogIn empLogInAct;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        empLogInAct = (empLogIn) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_emp_log_in, container, false);
        et = (EditText) v.findViewById(R.id.userEtFL2);
        et1 = (EditText) v.findViewById(R.id.passEtFL2);

        //sign in
        Button signIn = (Button) v.findViewById(R.id.signInFL2);

        signIn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String id = et.getText().toString();
                final String pass = et1.getText().toString();
                //final Intent intent = new Intent(getActivity(),MainMenu.class );
                //startActivity(intent);
                /*Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getContext(), "Login Success!", Toast.LENGTH_SHORT).show();
                        Log.d("TAG","sucess");
                        //startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getContext(), "Login failed!", Toast.LENGTH_SHORT).show();
                        Log.d("TAG","fail");
                    }
                });*/
                // empLogInAct.empLogScreen();



                Backendless.UserService.login(id, pass, new AsyncCallback<BackendlessUser>() {
                    @Override public void handleResponse(BackendlessUser response) {
                        Log.d("User", "Login complete " + response);
                        // Take user to Activity
                        Toast.makeText(getContext(),"Login success!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(),MainMenu.class );
                        startActivity(intent);
                        }
                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.d("User", "Login fail " + fault);
                            Toast.makeText(getContext(),"Login unsuccess!", Toast.LENGTH_SHORT).show();
                            // Stay in the Login Activity
                            }
                        });
                        }

        });
        //sign up
        Button signUp = (Button) v.findViewById(R.id.singUpFL2);
        signUp.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                empLogInAct.empSignupScreen();
            }
        });

        return v;
    }

    }
