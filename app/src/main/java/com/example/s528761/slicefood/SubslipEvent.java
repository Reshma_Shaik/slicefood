package com.example.s528761.slicefood;

/**
 * Created by s528761 on 10/27/2017.
 */
//Reshma Shaik created the file

public class SubslipEvent {
    private String eveid;
    private String evename;
    private String evedept;
    private String evedate;
    private String evetime;

    public SubslipEvent(String eveid, String evename, String evedept, String evedate, String evetime) {
        this.eveid = eveid;
        this.evename = evename;
        this.evedept = evedept;
        this.evedate = evedate;
        this.evetime = evetime;
    }

    public String getEvetime() {
        return evetime;
    }

    public void setEvetime(String evetime) {
        this.evetime = evetime;
    }

    public String getEveid() {
        return eveid;
    }

    public void setEveid(String eveid) {
        this.eveid = eveid;
    }

    public String getEvename() {
        return evename;
    }

    public void setEvename(String evename) {
        this.evename = evename;
    }

    public String getEvedept() {
        return evedept;
    }

    public void setEvedept(String evedept) {
        this.evedept = evedept;
    }

    public String getEvedate() {
        return evedate;
    }

    public void setEvedate(String evedate) {
        this.evedate = evedate;
    }

    @Override
    public String toString() {
        return "SubslipEvent{" +
                "eveid='" + eveid + '\'' +
                ", evename='" + evename + '\'' +
                ", evedept='" + evedept + '\'' +
                ", evedate='" + evedate + '\'' +
                '}';
    }
}
